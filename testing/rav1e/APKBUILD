# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=rav1e
pkgver=0.6.1
pkgrel=0
pkgdesc="The fastest and safest AV1 encoder"
url="https://github.com/xiph/rav1e"
arch="all !s390x" # no cargo for s390x
license="BSD-2-Clause custom"
makedepends="cargo cargo-c nasm"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-libs"
source="$pkgname-$pkgver.tar.gz::https://github.com/xiph/rav1e/archive/v$pkgver.tar.gz
	$pkgname-$pkgver.lock::https://github.com/xiph/rav1e/releases/download/v$pkgver/Cargo.lock
	"

# armhf: tests take >6h
[ "$CARCH" = armhf ] && options="$options !check"

# riscv64: enable textrels, tests take >6h
[ "$CARCH" = riscv64 ] && options="$options !check textrels"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_INCREMENTAL=false
export CARGO_PROFILE_RELEASE_DEBUG=false
export CARGO_PROFILE_RELEASE_LTO=true
export CARGO_PROFILE_RELEASE_PANIC=abort

prepare() {
	default_prepare

	ln -sfv "$srcdir"/$pkgname-$pkgver.lock Cargo.lock

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
	cargo cbuild \
		--release \
		--frozen \
		--prefix /usr \
		--library-type cdylib \
		--library-type staticlib
}

check() {
	cargo test --frozen --workspace
}

package() {
	cargo cinstall --release \
		--frozen \
		--prefix /usr \
		--destdir "$pkgdir" \
		--library-type cdylib \
		--library-type staticlib

	install -Dm755 target/release/rav1e -t "$pkgdir"/usr/bin/

	install -Dm644 README.md PATENTS -t "$pkgdir"/usr/share/doc/rav1e

	# fixes static linking flags
	sed -i 's|Libs.private:.*|Libs.private: -lssp_nonshared -lgcc_eh -lc|' \
		"$pkgdir"/usr/lib/pkgconfig/rav1e.pc
}

sha512sums="
3cef4cf83ee8429b3dc769480731c670dbdf4cf61be0675fa25661dcaf8e6a5f5bffa8bb340b951d55b3a3356a8e9102eea25881da01757dd74135c6eceab151  rav1e-0.6.1.tar.gz
7bf270b102caa71ca6688c2d365ca69da1b0262989b12378169fb7a0b2774b81750a0c9f515dabafc3012196cc508e1e881a71d91fc81b9fe9f3aa50c831d4a4  rav1e-0.6.1.lock
"
