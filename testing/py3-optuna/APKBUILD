# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-optuna
_pkgorig=optuna
pkgver=3.0.4
pkgrel=0
pkgdesc="Hyperparameter optimization framework"
url="https://optuna.org"
arch="noarch"
license="MIT"
depends="
	python3
	py3-alembic
	py3-colorlog
	py3-numpy
	py3-packaging
	py3-scipy
	py3-sqlalchemy
	py3-tqdm
	py3-yaml
	"
checkdepends="
	python3-dev
	py3-hypothesis
	py3-pytest
	py3-pytest-cov
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
options="!check" # several test dependencies are missing | skip fow now
source="https://github.com/optuna/optuna/archive/v$pkgver/$_pkgorig-$pkgver.tar.gz"
builddir="$srcdir/optuna-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer --destdir="$pkgdir" dist/*.whl
}

sha512sums="
54fb708a0921aef2768524bdcf41af9a8574e2fb15a0c820764ee2c7817dee4925a2b0cefcfbaa6051cdb56caa5316a3b07a1b937cc889a2cdfec5ff4a5703fe  optuna-3.0.4.tar.gz
"
