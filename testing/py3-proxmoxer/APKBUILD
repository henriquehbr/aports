# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-proxmoxer
pkgver=2.0.0
pkgrel=0
pkgdesc="Python wrapper for Proxmox API v2"
url="https://github.com/proxmoxer/proxmoxer"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="
	py3-mock
	py3-openssh-wrapper
	py3-paramiko
	py3-pytest
	py3-requests-toolbelt
	py3-responses
	py3-testfixtures
	"
source="https://github.com/proxmoxer/proxmoxer/archive/$pkgver/proxmoxer-$pkgver.tar.gz"
builddir="$srcdir/proxmoxer-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# current https tests require an ancient version of py3-requests
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
ed20c4733688b49233eeb64585375a6183a11498444cb69b7a580393c89b2d611169a0d4155b8416ac3e91ed2d0317cd510420a78a476243c4c1ce4cda3dbb72  proxmoxer-2.0.0.tar.gz
"
