# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=print-manager
pkgver=22.12.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://www.kde.org/applications/utilities/"
pkgdesc="A tool for managing print jobs and printers"
license="GPL-2.0-or-later"
makedepends="
	cups-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/print-manager-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d759cd8c6330f34fff6223c9c365ea3a58de01306741acf64686c98c90bb83c93971a69b81e24f8e1e7df6ad2a2fe3a3a7afda6ffae73dea4f4354fd60cef8a5  print-manager-22.12.0.tar.xz
"
