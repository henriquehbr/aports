# Maintainer: psykose <alice@ayaya.dev>
pkgname=limine
pkgver=4.20221214.0
pkgrel=0
pkgdesc="Advanced multiprotocol x86/x86_64 BIOS/UEFI Bootloader"
url="https://limine-bootloader.org"
# only aarch64/x86_64 supported, x86 is meh
arch="aarch64 x86_64"
license="BSD-2-Clause"
makedepends="
	clang
	coreutils
	lld
	llvm
	mtools
	nasm
	"
subpackages="
	$pkgname-dev
	$pkgname-cd:_cd
	$pkgname-deploy
	$pkgname-pxe
	$pkgname-sys
	$pkgname-32:_32
	$pkgname-64:_64
	$pkgname-aarch64:_64_arm
	"
source="https://github.com/limine-bootloader/limine/releases/download/v$pkgver/limine-$pkgver.tar.xz"
options="!check" # no tests in tarball

build() {
	./configure \
		--host=$CHOST \
		--prefix=/usr \
		--enable-all
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

deploy() {
	pkgdesc="$pkgdesc (limine-deploy bios installer)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/bin/limine-deploy
}

_cd() {
	pkgdesc="$pkgdesc (cd/efi files)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/share/limine/limine-cd*.bin
}

pxe() {
	pkgdesc="$pkgdesc (pxe executable)"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-sys=$pkgver-r$pkgrel"

	amove usr/share/limine/limine-pxe.bin
}

sys() {
	pkgdesc="$pkgdesc (sys file)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/limine.sys
}

_32() {
	pkgdesc="$pkgdesc (32-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTIA32.EFI
}

_64() {
	pkgdesc="$pkgdesc (64-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTX64.EFI
}

_64_arm() {
	pkgdesc="$pkgdesc (ARM 64-bit uefi image)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/share/limine/BOOTAA64.EFI
}

sha512sums="
61b16b5854f9a123c6e0d7cba17fcab285393ed13407b9288cba0c130ff30862e2f3c82f7e0f62c0b09bfc1d6aaac49e976656aba62d4046df37df4c901e12b5  limine-4.20221214.0.tar.xz
"
