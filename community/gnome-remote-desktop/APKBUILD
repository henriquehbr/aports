# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-remote-desktop
pkgver=43.2
pkgrel=0
pkgdesc="GNOME Remote Desktop - remove desktop server"
url="https://gitlab.gnome.org/GNOME/gnome-remote-desktop"
arch="all !s390x" # blocked by pipewire
license="GPL-2.0-or-later"
# mesa and libgudev are checkdepends, but they don't get installed
# due to options="!check"
makedepends="
	asciidoc
	cairo-dev
	fdk-aac-dev
	ffnvcodec-headers
	freerdp-dev
	fuse3-dev
	glib-dev
	libdrm-dev
	libepoxy-dev
	libgudev-dev
	libnotify-dev
	libsecret-dev
	libvncserver-dev
	libxkbcommon-dev
	mesa-dev
	meson
	pipewire-dev
	tpm2-tss-dev
	"
checkdepends="
	adwaita-icon-theme
	bash
	dbus
	gnome-settings-daemon
	mutter
	py3-dbus
	py3-gobject3
	xvfb-run
	"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Needs a full fat GNOME session
source="https://download.gnome.org/sources/gnome-remote-desktop/${pkgver%.*}/gnome-remote-desktop-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemd=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	mkdir -p /tmp/runtimedir
	GSETTINGS_SCHEMA_DIR=output/src/ XDG_RUNTIME_DIR=/tmp/runtimedir meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
e7e9c36e04a7273cb1700859d59ab8130bca7103c3def7f51a6df31b98f6021b9ceb15d2f9e71fef2464141dcd01d467e126731028d7dd6b6ec843dd52b1e8d4  gnome-remote-desktop-43.2.tar.xz
"
