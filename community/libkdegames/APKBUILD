# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkdegames
pkgver=22.12.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/applications/games"
pkgdesc="Common code and data for many KDE games"
license="LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kbookmarks-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdnssd-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kjobwidgets-dev
	knewstuff-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libsndfile-dev
	openal-soft-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdegames-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang carddecks::noarch"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

carddecks() {
	pkgdesc="Contains all carddecks for KDE cardgames"
	depends="$pkgname"

	amove usr/share/carddecks
}

sha512sums="
a030a2d9a46bdd5dbd49b8c25a4a79385307ff2099e1c9edad251224b59af4c77da66f710ee3f2bb18b9cf6570c4ac0a0c85454341553baa49ccab06ff4d4179  libkdegames-22.12.0.tar.xz
"
