# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-pyproject-api
pkgver=1.2.1
pkgrel=0
pkgdesc="Python API to interact with the python pyproject.toml based projects"
url="https://github.com/tox-dev/pyproject-api"
arch="noarch"
license="MIT"
depends="python3 py3-packaging"
makedepends="
	py3-gpep517
	py3-hatch-vcs
	py3-hatchling
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-pytest-mock
	py3-virtualenv
	py3-wheel
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/tox-dev/pyproject-api/archive/refs/tags/$pkgver.tar.gz
	no-python2.patch
	"
builddir="$srcdir/pyproject-api-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
bc84a0aa1845cf910a980a2fb562553eb3e58f6b16f3516fccc46c9723dca8ac553c457160a1fcb39d4de984906a196dd9f625fcae1ee6295e30b3f9a2618c6d  py3-pyproject-api-1.2.1.tar.gz
8e5bfccbf7471991fcf64cd521c47e42e3a7ad181482a739a4b4d3d9330da0df2bdf2374ed001c6a15cea967f8767c50d01c7d924dd9a82156c293f242f8fa8f  no-python2.patch
"
