# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ark
pkgver=22.12.0
pkgrel=0
pkgdesc="Graphical file compression/decompression utility with support for multiple formats"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.ark"
license="GPL-2.0-only"
depends="
	7zip-virtual
	lrzip
	unzip
	zip
	zstd
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kparts-dev
	kpty-dev
	kservice-dev
	kwidgetsaddons-dev
	libarchive-dev
	libzip-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	xz-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/ark-$pkgver.tar.xz
	0001-fix-build.patch
	"
subpackages="$pkgname-doc $pkgname-lang"

# secfixes:
#   20.08.0-r1:
#     - CVE-2020-24654
#   20.04.3-r1:
#     - CVE-2020-16116

case "$CARCH" in
	# FIXME: tests fail, see alpine/aports#14266
	x86_64) options="!check";;
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run -a ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
9430a1ab9fc82eefcd4dec8df02fd141e3506c1a9bcc91fd613e52db35fbebaefbd2977ca6d1106deb93e5254ef72e71b3a1bce2b115136b4bca57cd615ef64d  ark-22.12.0.tar.xz
1e710c014eb37ebd93df8b18c18d7ee9b997e2d8807debf45bc5b0c0506ced53f10c125f576676a61c91725f328ab56b5728d48593ca4cb08dad040a43912460  0001-fix-build.patch
"
