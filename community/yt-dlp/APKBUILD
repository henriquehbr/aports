# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Sodface <sod@sodface.com>
pkgname=yt-dlp
pkgver=2022.11.11
pkgrel=1
pkgdesc="Command-line program to download videos from YouTube"
url="https://github.com/yt-dlp/yt-dlp"
arch="noarch"
license="Unlicense"
depends="python3 py3-mutagen py3-websockets py3-certifi py3-brotli"
makedepends="py3-setuptools"
checkdepends="py3-flake8 py3-nose py3-pytest"
subpackages="
	$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/yt-dlp/yt-dlp/releases/download/$pkgver/yt-dlp.tar.gz"
builddir="$srcdir/$pkgname"

build() {
	python3 setup.py build

	make completions
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Install fish completion to the correct directory
	rm -r "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 completions/fish/yt-dlp.fish \
		-t "$pkgdir"/usr/share/fish/completions
}

sha512sums="
915b2bbee2ee571b9828b013faaec2576091cd31e27cf4740d2b12b9bab9e019f0c271c2783a7931513debe3d8e982f38d94db3e86710673d4348d06a7213f30  yt-dlp-2022.11.11.tar.gz
"
