# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kopete
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/Kopete"
pkgdesc="An instant messenger supporting AIM, ICQ, Jabber, Gadu-Gadu, Novell GroupWise Messenger, and more"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kconfig-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdnssd-dev
	kdoctools-dev
	kemoticons-dev
	khtml-dev
	ki18n-dev
	kidentitymanagement-dev
	kitemmodels-dev
	knotifyconfig-dev
	kparts-dev
	ktexteditor-dev
	kwallet-dev
	libkleo-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopete-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f53bfbf88ede9106c50f496569df8a3e59778770a04cb76ce93a627a2518486e43e7f439ec3b7590b251a2a13bd2f6ae536649abb2992e9455cdca7e9d4a5470  kopete-22.12.0.tar.xz
"
