# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmenuedit
pkgver=5.26.4
pkgrel=0
pkgdesc="KDE menu editor"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemviews-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	sonnet-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
287f1c8dbfe0293f954f43cc55dd65b805cc668770ca0b657da0de16fcf7dea2a99040003b5da8e1445f65760adce8e7e346d7fa3b2bee2c4b2eedd5c81aa26c  kmenuedit-5.26.4.tar.xz
"
