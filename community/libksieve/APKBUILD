# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libksieve
pkgver=22.12.0
pkgrel=0
pkgdesc="KDE PIM library for managing sieves"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-only"
depends_dev="
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kimap-dev
	kio-dev
	kmailtransport-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksieve-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# libksieveui-findbar-findbarbasetest, sieveeditorhelphtmlwidgettest and sieveeditor-autocreatescripts-sieveeditorgraphicalmodewidgettest require OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(libksieveui-findbar-findbarbase|sieveeditorhelphtmlwidget|sieveeditor-autocreatescripts-sieveeditorgraphicalmodewidget)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bbcfa42c84d5ab97a82d68e1454db7b8983dfdb4b964f76c29b6868c975dcb1dc077b7eed696404ab85e6da91ad1e9d923ba4a974820b0428cc117d3ff83fa87  libksieve-22.12.0.tar.xz
"
