# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ffmpegthumbs
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="FFmpeg-based thumbnail creator for video files"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ffmpeg-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	taglib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ffmpegthumbs-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9d5f900b46666af510277e5ba46218633d68b45494de7843d44d9316b8108913c34d2f2f30925646e068846d7b0ad5f5b846698fd297ee27e1651eb9741edcbb  ffmpegthumbs-22.12.0.tar.xz
"
