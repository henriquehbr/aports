# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeplasma-addons
pkgver=5.26.4
pkgrel=0
pkgdesc="All kind of addons to improve your Plasma experience"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.0-only AND GPL-2.0-or-later"
depends="purpose"
depends_dev="
	icu-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdeclarative-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	krunner-dev
	kservice-dev
	kunitconversion-dev
	kwindowsystem-dev
	networkmanager-qt-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	samurai
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# converterrunnertest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "converterrunnertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9434ee120ef4e5e61781f63c9b3e8909612f4083fe60e5593eda7395b7956e6ee2c45cfa922aaf1a68570144146a56926da60720b83b961e5cf524275b9be0ee  kdeplasma-addons-5.26.4.tar.xz
"
