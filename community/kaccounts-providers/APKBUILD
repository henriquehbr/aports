# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaccounts-providers
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later"
depends="
	signon-plugin-oauth2
	"
makedepends="
	extra-cmake-modules
	intltool
	kaccounts-integration-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kpackage-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-providers-$pkgver.tar.xz"
options="!check" # No tests
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
20a4470bb5aefe197807aea11ccb94811dc3f9182cade669ae6529643c7bbe17ef50cc50e4b47ba1e0bd020719ba8d6a8ad6365c44d3e82cce4970769c30dc44  kaccounts-providers-22.12.0.tar.xz
"
