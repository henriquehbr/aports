# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=xwayland
pkgver=22.1.6
pkgrel=0
pkgdesc="Compact xserver to run under wayland for compatibility"
url="https://www.x.org/wiki"
arch="all"
license="MIT"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc"
depends="xorg-server-common xkbcomp"
depends_dev="
	dbus-dev
	font-util-dev
	libdrm-dev
	libepoxy-dev
	libtirpc-dev
	libmd-dev
	libxcvt-dev
	libxfont2-dev
	libxkbfile-dev
	mesa-dev
	pixman-dev
	wayland-dev
	wayland-protocols
	xorgproto
	xtrans
	"
makedepends="
	$depends_dev
	meson
	"
source="https://xorg.freedesktop.org/archive/individual/xserver/xwayland-$pkgver.tar.xz
	use-libtirpc-nokrb.patch
	"

replaces="xorg-server-xwayland"

# secfixes:
#   22.1.6-r0:
#     - CVE-2022-4283
#     - CVE-2022-46340
#     - CVE-2022-46341
#     - CVE-2022-46342
#     - CVE-2022-46343
#     - CVE-2022-46344
#   21.1.4-r0:
#     - CVE-2021-4008
#     - CVE-2021-4009
#     - CVE-2021-4010
#     - CVE-2021-4011
#   21.1.0-r4:
#     - CVE-2021-3472

build() {
	abuild-meson \
		-Db_lto=true \
		-Dipv6=true \
		-Dxvfb=false \
		-Dxdmcp=false \
		-Dxcsecurity=true \
		-Ddri3=true \
		-Dxwayland_eglstream=false \
		-Dglamor=true \
		-Dsha1=libmd \
		-Dxkb_dir=/usr/share/X11/xkb \
		-Dxkb_output_dir=/var/lib/xkb \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	# Part of xorg-server-doc
	rm -f "$pkgdir"/usr/share/man/man1/Xserver.1

	# Part of xorg-server-common
	rm -f "$pkgdir"/usr/lib/xorg/protocol.txt
}

sha512sums="
a78f44f14a6c1e25afbf245c5c757a253f0607afdc80c8b852cf6f810247566d1cc17b63d4442a21d8e69bdb696faf1014438ad98a977fbed9a11b9c5b85e0a4  xwayland-22.1.6.tar.xz
db6fce62096fc969a503e2660bd511b50bb578a77ac2969ec93b2442c9ee3866ec93fa8bf78f3bd1bbf0ecc8b4e74d3b5e210b3442da3a3841c6c6214372d617  use-libtirpc-nokrb.patch
"
