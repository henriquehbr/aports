# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=flatbuffers
pkgver=22.12.06
pkgrel=0
pkgdesc="Memory Efficient Serialization Library"
url="https://google.github.io/flatbuffers/"
# armhf: blocked by bus error in tests
# s390x: segfaults in tests
arch="all !armhf !s390x"
license="Apache-2.0"
depends_dev="flatc=$pkgver-r$pkgrel"
makedepends="cmake samurai"
subpackages="$pkgname-dev flatc"
source="flatbuffers-$pkgver.tar.gz::https://github.com/google/flatbuffers/archive/v$pkgver.tar.gz
	locale-headers.patch
	"

# Bus error in armv7 as well but it has downstream users
case "$CARCH" in
	armv7) options="!check"
esac

# secfixes:
#   0:
#     - CVE-2020-35864

build() {
	cmake -B . -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DFLATBUFFERS_BUILD_SHAREDLIB=ON \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build .
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install .
	install -Dm755 flatc -t "$pkgdir"/usr/bin/
	rm "$pkgdir"/usr/lib/*.a
}

flatc() {
	pkgdesc="$pkgdesc (compiler)"

	amove usr/bin
}

sha512sums="
1b2c8eaa2ebeb92c3ebc363888b34b817d7b62a568e40bce2b037814f1f0103d4cb7db8507ec9ca05421be61b0b2be3edd6c4d564f75d1ce39278d73f8442ac1  flatbuffers-22.12.06.tar.gz
756f338938b9064366744ad4bb67488f968d8921f688607ca7796af93264d005638941abc6cf1d0275f07eaee920a13e1073541dbff28f0f7d030692a36574d0  locale-headers.patch
"
