# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knights
pkgver=22.12.0
pkgrel=0
pkgdesc="Chess board by KDE with XBoard protocol support"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/games/knights/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kio-dev
	kplotting-dev
	ktextwidgets-dev
	kwallet-dev
	kxmlgui-dev
	libkdegames-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/knights-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3756c5e634c745df3df5df4d48a0a5adff64f40847ab5c39f57de87f967eea0cb60c98a2c317d9370be35f8b3b89756f0dbe829fe32fb72342a65ad561d9d680  knights-22.12.0.tar.xz
"
