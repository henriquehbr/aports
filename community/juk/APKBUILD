# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=juk
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://juk.kde.org/"
pkgdesc="A jukebox, tagger and music collection manager"
license="GPL-2.0-or-later"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	taglib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/juk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7d20899fbb8b53e70710d862bdfd9afbd8d7e8860879d0b00fe44040f6467e9b4f9401caee91e994bac03c7ab0ad7c6b68eaa236cc0683b4e13171d190dc0315  juk-22.12.0.tar.xz
"
