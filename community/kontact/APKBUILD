# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontact
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Container application to unify several major PIM applications within one application"
license="GPL-2.0-or-later"
makedepends="
	akonadi-dev
	extra-cmake-modules
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kontactinterface-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontact-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
adb397d2675a5e012a14da25c733666a6ec8d57da1bdb0eb4bc5f3af50e8dfae289d310d5e84d40e7b4b2db1841f1b838ff1310fd0ab39845b6b0dfc88a8430f  kontact-22.12.0.tar.xz
"
